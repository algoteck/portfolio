import "bootstrap/dist/css/bootstrap.min.css";

import { Header } from "./components";
import { Showcase, Skillset, Introduction, Projectintro } from "./pages";

function App() {
  return (
    <>
      <Header />
      <Showcase />
      <Introduction />
      <Skillset />
      <Projectintro />
    </>
  );
}

export default App;
