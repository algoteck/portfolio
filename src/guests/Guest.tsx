import React, { useState, useRef, useEffect } from "react";
import { Guests, FindGuest, FoundGuest } from "./Guests";

const Guest: React.FC<{}> = () => {
  const [name, setName] = useState<string>("");
  const [guestName, setGuestName] = useState<string | undefined>();
  const [foundGuest, setFoundGuest] = useState<string | undefined>();
  const [guests, setGuests] = useState<string[]>([]);

  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputRef.current?.focus();
  }, []);
  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setName(e.currentTarget.value);
  };

  const onChangeFindGuestHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setGuestName(e.currentTarget.value);
  };

  const btnHandler = () => {
    setName("");
    setGuests([...guests, name]);
    console.log(guests);
  };

  const btnHandlerFindGuest = () => {
    setFoundGuest(guests.find((guest) => guest === guestName));
  };
  return (
    <>
      <Guests
        name={name}
        onChange={onChangeHandler}
        btnHandler={btnHandler}
        guestsList={guests}
        inputRef={inputRef}
      />
      <FindGuest
        guestName={guestName}
        onChange={onChangeFindGuestHandler}
        btnHandler={btnHandlerFindGuest}
      />
      {foundGuest ? <FoundGuest foundGuest={foundGuest} /> : null}
    </>
  );
};

export default Guest;
