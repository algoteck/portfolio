import React from "react";

type Props = {
  name: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  btnHandler: () => void;
  guestsList: string[];
  inputRef: React.MutableRefObject<HTMLInputElement | null>;
};

type FindProps = {
  guestName: string | undefined;
  btnHandler: () => void;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

type FoundProps = {
  foundGuest: string | undefined;
};

export const Guests: React.FC<Props> = ({
  name,
  onChange,
  btnHandler,
  guestsList,
  inputRef,
}) => {
  return (
    <div>
      <h1>Add new Guest</h1>
      <input value={name} onChange={onChange} ref={inputRef} />
      <button onClick={btnHandler}>Add Guest</button>
      <h3>----------------------------</h3>
      <h1>List of Guests</h1>
      <ul>
        {guestsList.map((guestName, key) => {
          return <li key={key}>{guestName}</li>;
        })}
      </ul>
    </div>
  );
};

export const FindGuest: React.FC<FindProps> = ({
  guestName,
  onChange,
  btnHandler,
}) => {
  return (
    <>
      <h3>----------------------------</h3>
      <h5>Find Guest Name</h5>
      <input value={guestName} onChange={onChange} />
      <button onClick={btnHandler}>Find Guest</button>
    </>
  );
};

export const FoundGuest: React.FC<FoundProps> = ({ foundGuest }) => {
  return (
    <>
      <h6>Guest found</h6>
      <h6>{foundGuest}</h6>
    </>
  );
};
