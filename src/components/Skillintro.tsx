import "./style-skillintro.scss";

import { Container } from "react-bootstrap";

interface skillintro {
  heading1: string;
  heading2: string;
  text_body: string;
}

export const Skillintro: React.FC<skillintro> = ({
  heading1,
  heading2,
  text_body,
}) => {
  return (
    <>
      <section className="bg-dark intro-section">
        <Container className="intro-container">
          <h1 className="introduction-heading">
            {heading1} <span className="myname">{heading2}</span>
          </h1>
          <p className="introduction-subheading">{text_body}</p>
        </Container>
      </section>
    </>
  );
};
