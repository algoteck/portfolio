import { Navbar, Container, Nav, Button } from "react-bootstrap";
import "./style.css";
import Dplogo from "../assests/dp_logo.svg";

export const Header: React.FC<{}> = () => {
  return (
    <>
      <Navbar bg="dark" expand="lg" variant="dark" fixed="top">
        <Container>
          <Navbar.Brand href="#home">
            <img src={Dplogo} alt="Dplogo" className="dplogo" />
          </Navbar.Brand>
          <h1 className="my-name d-none d-md-block">
            Muhammad Junaid Iftikhar
          </h1>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />

          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link href="#home" className="mx-auto">
                Home
              </Nav.Link>
              <Nav.Link href="#link" className="mx-auto">
                Link
              </Nav.Link>
              <Button
                variant="outline-secondary"
                className="mx-auto say-hello "
              >
                Say Hello
              </Button>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};
