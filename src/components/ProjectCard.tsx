import { Card, Container } from "react-bootstrap";
import "./style-projectcard.scss";
interface skills {
  url_image: string;
  heading1: string;
  heading2: string;
  heading3: string;
  text1: string;
  text2: string;
  text3: string;
}

export const ProjectCard: React.FC<skills> = ({
  url_image,
  heading1,
  heading2,
  heading3,
  text1,
  text2,
  text3,
}) => {
  return (
    <>
      <Card className="card-skillcard">
        <Card className="card-projectcard__content text-light">
          <Card.Img
            variant="top"
            src={url_image}
            className="card-projectcard__img"
          />
          <Card.Body className="card-projectcard__body">
            <Card.Title className="card-projectcard__heading1">
              {heading1}
            </Card.Title>
            <Card.Text className="card-projectcard__text1">{text1}</Card.Text>
          </Card.Body>
        </Card>
      </Card>
    </>
  );
};

{
  /* <div className="card-container">
  <div className="card-skillcard">
    <div className="card-skillcard__content text-light">
      <h3 className="card-skillcard__header">Card 1</h3>
      <p className="card-skillcard__info">
        Lorem ipsum dolor sit amet consectetur adipisicing elit.
      </p>
      <button className="card__button btn ">Read up</button>
    </div>
  </div>
</div>; */
}
