import { ProjectCard } from "../../components";
import fullstack from "../../assests/fullstack.svg";

export const ProjectDetail: React.FC<{}> = () => {
  return (
    <>
      <ProjectCard
        url_image={fullstack}
        heading1="Full Stack Developer"
        text1="I prefer learning new technologies, skills and languages. I love to use new tools."
        heading2="Things I enjoy developing:"
        text2="Frontend Apps, Backend Apps, Web Applications, React Native, PWA"
        heading3="Programming Languages:"
        text3="NodeJS, JavaScript, Express
                Python, Django, FastAPI
                Python ML, AI, Data Visualisation
                PHP, Lavavel
                Automation"
      />
    </>
  );
};
