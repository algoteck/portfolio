import { Skillintro } from "../../components";

export const Introduction: React.FC<{}> = () => {
  return (
    <>
      <Skillintro
        heading1="Hi I'm"
        heading2="Muhammad Junaid Iftikhar"
        text_body="Since beginning my journey as a freelance designer nearly 10 years
            ago, I've done remote work for agencies, consulted for startups, and
            collaborated with talented people to create digital products for
            both business and consumer use. I'm quietly confident, naturally
            curious, and perpetually working on improving my chops one design
            problem at a time."
      />
    </>
  );
};
