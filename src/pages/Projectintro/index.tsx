import { Row, Col, Container, Card } from "react-bootstrap";
import { Skillintro } from "../../components";
import { ProjectDetail } from "../ProjectDetail";
import "./style.scss";

export const Projectintro: React.FC<{}> = () => {
  return (
    <>
      <section className="bg-dark projectsection">
        <Container>
          <Row>
            <Col xl={12}>
              <Skillintro
                heading1="My Portfolio"
                heading2="Projects"
                text_body="I'm a bit of a digital product junky. Over the years, I've used hundreds of web and mobile apps in different industries and verticals. Eventually, I decided that it would be a fun challenge to try designing and building my own. 'm a bit of a digital product junky. Over the years, I've used hundreds of web and mobile apps in different industries and verticals. Eventually, I decided that it would be a fun challenge to try designing and building my own. 'm a bit of a digital product junky. Over the years, I've used hundreds of web and mobile apps in different industries and verticals. Eventually, I decided that it would be a fun challenge to try designing and building my own."
              />
            </Col>
            <Col xl={12}>
              <div className="d-lg-flex project-list">
                <span className="p-2">
                  <ProjectDetail />
                </span>
                <span className="p-2">
                  <ProjectDetail />
                </span>
                <span className="p-2">
                  <ProjectDetail />
                </span>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};
