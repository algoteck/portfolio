import { Container } from "react-bootstrap";
import "./showcase-style.css";
import UserLogo from "../../assests/avatar.svg";
import Hero from "../../assests/hero.svg";

export const Showcase: React.FC<{}> = () => {
  return (
    <>
      <section className="bg-light ">
        <Container className="showcase-container">
          <h1 className="text-center header-line">
            Full-Stack Developer & Software Engineer
          </h1>
          <h1 className="text-center header-line-sub">
            TypeScript | NodeJs | React | Express | Python
          </h1>
          <img src={UserLogo} alt="React Logo" className="userlogo" />
          <img src={Hero} alt="Hero Logo" className="img-fluid herologo" />
        </Container>
      </section>
    </>
  );
};
