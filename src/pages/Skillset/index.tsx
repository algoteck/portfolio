import { Row, Col, Container } from "react-bootstrap";
import { SkillCard } from "../../components";
import fullstack from "../../assests/fullstack.svg";
import designer from "../../assests/designer.svg";
import devops from "../../assests/devops.svg";
import "./skillset-style.css";
export const Skillset: React.FC<{}> = () => {
  return (
    <>
      <section className="bg-white section-skillset">
        <Container>
          <Row className="row-skillset g-3">
            <Col xs className="col-skillset">
              <SkillCard
                url_image={fullstack}
                heading1="Full Stack Developer"
                text1="I prefer learning new technologies, skills and languages. I love to use new tools."
                heading2="Things I enjoy developing:"
                text2="Frontend Apps, Backend Apps, Web Applications, React Native, PWA"
                heading3="Programming Languages:"
                text3="NodeJS, JavaScript, Express
                Python, Django, FastAPI
                Python ML, AI, Data Visualisation
                PHP, Lavavel
                Automation"
              />
            </Col>
            <Col xs className="col-skillset ">
              <SkillCard
                url_image={designer}
                heading1="Designer"
                text1="I value simple content structure, clean design patterns, and thoughtful interactions."
                heading2="Things I enjoy designing:"
                text2="UX, UI, Web, Apps, Logos, Adobe Photoshop, Figma"
                heading3="Design Tools:"
                text3="Balsamiq Mockups, Figma, Invision, Marvel, Pen & Paper"
              />
            </Col>
            <Col xs className="col-skillset ">
              <SkillCard
                url_image={devops}
                heading1="DevOps"
                text1="I have experience with Amazon Web Service - AWS, Google Cloud Platform - GCP"
                heading2="Things I love to Maintain:"
                text2="Web-Applications, RabbitMQ, Redis, RDMS, MongoDB"
                heading3="DevOps Skills:"
                text3="AWS, Google Cloud Platfor, Elastic beans S3, CI/CD Setup"
              />
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};
