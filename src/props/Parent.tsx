import { Child } from "./Child";
import { Header } from "./Header";

const Parent: React.FC<{}> = () => {
  function btnHandler() {
    console.log("Clicked");
  }
  return (
    <Child name="Juni" onClick={btnHandler}>
      <Header />
    </Child>
  );
};

export default Parent;
