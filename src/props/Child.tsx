import { Children } from "react";

interface Props {
  name: string;
  onClick: () => void;
}

export const Child: React.FC<Props> = ({ name, onClick, children }) => {
  return (
    <div>
      {children}
      <h1>{name}</h1>
      <button onClick={onClick}>Click me</button>
    </div>
  );
};
